#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import math
import itertools as itl
import csv
import matplotlib.pyplot as plt

from collections import Counter
from apyori import apriori
from pandas.io.json import json_normalize


# In[2]:


basketData = pd.read_csv('groceries.csv', header=None)
basketList = basketData.values.tolist()


# In[3]:


cleanList =  [[x for x in set(y) if ((isinstance(x, str) and x != 'nan') or not math.isnan(x))] for y in basketList]


# In[4]:


items = list(itl.chain.from_iterable(cleanList))
itCnt = dict(Counter(items))


# In[5]:


rules = apriori(cleanList, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)


# In[6]:


ruleList = list(rules)


# In[7]:


ruleJson = [{
    'BasketItems': ','.join([str(x) for x in rule.items]),
    'Support': rule.support,
    'Antecedent': ','.join([str(y) for y in rule.ordered_statistics[0].items_base]),
    'Consequent': ','.join([str(z) for z in rule.ordered_statistics[0].items_add]),
    'Confidence': rule.ordered_statistics[0].confidence,
    'Lift': rule.ordered_statistics[0].lift
} for rule in ruleList]


# In[8]:


ruleDf = json_normalize(ruleJson)
ruleDf


# In[20]:


topNByLiftAndConfidence = ruleDf.sort_values(by=['Lift', 'Confidence'], ascending=False)[:20]
plt.bar(topNByLiftAndConfidence.BasketItems, topNByLiftAndConfidence.Support)
plt.xticks(rotation='vertical')
plt.xlabel('Top Rules')
plt.ylabel('Support')
plt.show()


# In[19]:


topNBySupport = ruleDf.sort_values(by='Support', ascending=False)
plt.scatter(topNBySupport.Lift, topNBySupport.Confidence)
plt.xlabel('Lift')
plt.ylabel('Confidence')
plt.show()


# In[22]:


topNItems = {k: v for k, v in sorted(itCnt.items(), key=lambda item: item[1], reverse=True)[:20]}
plt.bar(range(len(topNItems)), list(topNItems.values()), align='center')
plt.xticks(range(len(topNItems)), list(topNItems.keys()), rotation='vertical')
plt.xlabel('Items')
plt.ylabel('Quantity Sold')
plt.show()


# In[23]:


with open('items.csv','w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(('Product','Count'))
    for k in itCnt:
        csv_out.writerow((k,itCnt[k]))


# In[24]:


with open('Rules.csv','w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(('RuleNo.', 'BasketItems', 'Support', 'Antecedent', 'Consequent', 'Confidence', 'Lift'))
    ix = 1
    for r in ruleList:
        test = r
        sItems = ','.join([str(x) for x in test.items])
        sSupport = test.support
        sOrdSt = test.ordered_statistics[0]
        sAntecedent = ','.join([str(x) for x in sOrdSt.items_base])
        sConsequent = ','.join([str(x) for x in sOrdSt.items_add])
        sConfidence = sOrdSt.confidence
        sLift = sOrdSt.lift
        csv_out.writerow(['Rule_'+str(ix), sItems, sSupport, sAntecedent, sConsequent, sConfidence, sLift])
        ix += 1


# In[ ]:




