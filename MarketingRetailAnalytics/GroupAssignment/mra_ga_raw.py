#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 11:59:36 2020

@author: sabari
"""

import pandas as pd
import math
import itertools as itl
import csv

from collections import Counter
from apyori import apriori

#Read data from csv file.
basketData = pd.read_csv('groceries.csv', header=None)

#Convert dataframe to list of lists
basketList = basketData.values.tolist()

#Remove nan`s from the basketList
cleanList =  [[x for x in y if ((isinstance(x, str) and x != 'nan') or not math.isnan(x))] for y in basketList]

items = list(itl.chain.from_iterable(cleanList))

#This gives the individual items sold along with the count.
itCnt = dict(Counter(items))

with open('items.csv','w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(('Product','Count'))
    for k in itCnt:
        csv_out.writerow((k,itCnt[k]))

rules = apriori(cleanList, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)
ruleList = list(rules)

with open('Rules.csv','w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(('RuleNo.', 'BasketItems', 'Support', 'Antecedent', 'Consequent', 'Confidence', 'Lift'))
    ix = 1
    for r in ruleList:
        test = r
        sItems = ','.join([str(x) for x in test.items])
        sSupport = test.support
        sOrdSt = test.ordered_statistics[0]
        sAntecedent = ','.join([str(x) for x in sOrdSt.items_base])
        sConsequent = ','.join([str(x) for x in sOrdSt.items_add])
        sConfidence = sOrdSt.confidence
        sLift = sOrdSt.lift
        csv_out.writerow(['Rule_'+str(ix), sItems, sSupport, sAntecedent, sConsequent, sConfidence, sLift])
        ix += 1

