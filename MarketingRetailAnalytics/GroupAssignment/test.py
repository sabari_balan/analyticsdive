#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 16:06:17 2020

@author: sabari
"""

s=frozenset([1,2,3,4,5])
print(type(s), s)
print(','.join([str(x) for x in s]))
