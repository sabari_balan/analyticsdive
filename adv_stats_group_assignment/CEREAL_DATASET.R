cerel_Data=read.csv(file.choose(), header = TRUE)
summary(cerel_Data)
str(cerel_Data)
boxplot(cerel_Data)

# since the question itself says five point likert scale we could able to see max values as 6 in some of the attributes. 
# those 6 are replaced by 5 
cerel_Data[cerel_Data==6]=5
boxplot
## Correlation matrix
cerel_data_1=cerel_Data[,2:26]
cerel_data_1

correlation_matrix=(cor(cerel_data_1)) # Corrleation matrix

library(nFactors)
?nFactors

ev=eigen(cor(cerel_data_1))
ev
eigen_value=ev$values
eigen_value=ev$vectors
eigen_value

#scree plot
Factor=c(1:25)
Scree=data.frame(Factor,eigen_value)
Scree
plot(Scree,main="Scree Plot", col="Blue",ylim=c(0,4))
lines(Scree,col="Red")

library(psych)

#PCA
cerel_data_pca = princomp(cerel_data_1,scores = TRUE, cor = TRUE)
summary(cerel_data_pca)
PCA_loadings=loadings(cerel_data.pca)
Unrotate=principal(cerel_data_1, nfactors=5, rotate="none")
print(Unrotate,digits=3)

#FA
factanal(cerel_data_1,factors = 5,rotation = "none")
factanal(cerel_data_1,factors = 5,rotation = "varimax")

