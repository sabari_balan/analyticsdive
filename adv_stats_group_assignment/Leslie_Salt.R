# Install missing required packages.
packages <- c("xlsx","car","rstudioapi")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))
}

#load all the required packages.
lapply(packages, require, character.only = T)

#set working directory to the current editor file's directory
setwd(dirname(rstudioapi::getSourceEditorContext()$path))

#Assuming the data set file is in the same directory as the editor file, loading the file.
file_name = file.path(getwd(),"Dataset_LeslieSalt.xlsx")

data = read.xlsx(file_name, sheetIndex = 1)

attach(data)

names(data)

str(data)

summary(data)

scaled_data = scale(data)
boxplot(scaled_data)

# boxplot(Price)
# boxplot(County)
# boxplot(Size)
# boxplot(Elevation)
# boxplot(Sewer)
# boxplot(Date)
# boxplot(Flood)
# boxplot(Distance)

# plot(Price, County)
# abline(lm(Price~County))
# plot(Price, Size)
# abline(lm(Price~Size))
# plot(Price, Elevation)
# abline(lm(Price~Elevation))
# plot(Price, Sewer)
# abline(lm(Price~Sewer))
# plot(Price, Date)
# abline(lm(Price~Date))
# plot(Price, Flood)
# abline(lm(Price~Flood))
# plot(Price, Distance)
# abline(lm(Price~Distance))

cor(data)

priceLM = lm(Price~County+Size+Elevation+Sewer+Date+Flood+Distance, data = data)
print(priceLM, digits = 10)

confint(priceLM)
summary(priceLM)

finalPriceModel = lm(Price~County+Sewer+Flood, data = data)

summary(finalPriceModel)

anova(finalPriceModel)

fPredictedPrice = predict(finalPriceModel)
plot(Price)
plot(fPredictedPrice)
lines(Price, col='red')
lines(fPredictedPrice, col='blue')


anova(priceLM)

predictedPrice = predict(priceLM)
plot(Price)
plot(predict(priceLM))
lines(Price, col='red')
lines(predict(priceLM), col='blue')

lesliePropInfo = data.frame(Size=246.8, Flood=1, Distance=0, Elevation=0, Date=0, Sewer = 0, County = 0)
summary(lesliePropInfo)

predictedLesliePrice = predict(priceLM, lesliePropInfo, interval = 'confidence', level = 0.95)
predictedLesliePrice

fPredictedLesliePrice = predict(finalPriceModel, lesliePropInfo, interval = 'confidence', level = 0.95)
fPredictedLesliePrice

detach(data)
