
mydata=read.csv(file.choose(), header=TRUE)
View(mydata)
attach(mydata)
mydata=mydata[,2:8]
library(nFactors)
ev = eigen(cor(mydata)) # get eigenvalues
ev
EigenValue=ev$values
EigenValue
Factor=c(1,2,3,4,5,6,7)
Scree=data.frame(Factor,EigenValue)
plot(Scree,main="Scree Plot", col="Blue",ylim=c(0,4))
lines(Scree,col="Red")
library(psych)
Unrotate=principal(mydata, nfactors=3, rotate="none")
View(Unrotate)
print(Unrotate,digits=3)
UnrotatedProfile=plot(Unrotate,row.names(Unrotate$loadings))
Rotate=principal(mydata,nfactors=3,rotate="varimax")
print(Rotate,digits=6)
RotatedProfile=plot(Rotate,row.names(Rotate$loadings),cex=1.0)

######Test Prob#####
library('xlsx')
tpData= read.xlsx(file.choose(), header = T, sheetIndex = 1)
View(tpData)

attach(tpData)
names(tpData)

class(tpData)

typeof(tpData)
summary(tpData)

tpData = tpData[,2:7]
tpEv = eigen(cor(tpData))
tpEv

tpEValues = tpEv$values

tpFactors = factor(1:6)
tpFactors

tpScree = data.frame(tpFactors, tpEValues)
plot(tpScree,main="Scree Plot", col="Blue",ylim=c(0,4))
lines(tpScree,col="Red")

Unrotate=principal(tpData, nfactors=3, rotate="none")
View(Unrotate)
print(Unrotate,digits=3)
UnrotatedProfile=plot(Unrotate,row.names(Unrotate$loadings))
Rotate=principal(tpData,nfactors=3,rotate="varimax")
print(Rotate,digits=6)
RotatedProfile=plot(Rotate,row.names(Rotate$loadings),cex=1.0)



