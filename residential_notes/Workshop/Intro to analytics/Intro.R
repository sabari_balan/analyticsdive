x = c(5,25,30,20,15)
y = c(10,20,30,40,50)
z = c(50,60,70,75,80)

a = rep(1:5, times = 2)
b = rep(c(10,20,30), times = 3)
c = rep(c(20,25,22,23), times = 4)

d = rep(1:3, each = 2)
e = rep(c(25,30,35), each = 4)

f = seq(from=1, to=20, by=2)
g = seq(from=2, to=30, by=3)
h = seq(from=50, to=30, by=-4)
i = seq(from=50, to=18, by=-5)

j = c(101:110)

length(x);length(a);length(c);length(e);length(i)

mode(x);

k = sort(x, decreasing = TRUE);
l = sort(y);

min(x);max(a)

x1 = sum(x);

sum(c); sum(d)

x2 = x + 5

total = x + y;
diff = x - y;



m1 = matrix(1:9, nrow=3, ncol=3)
m2 = matrix(c(10,20,30,40,50,60), nrow = 2, ncol = 3)

m3 = matrix(1:9, nrow = 3, ncol = 3, byrow = TRUE)

m4 = matrix(seq(from = 2, to = 12, by = 2), 3, 2, byrow = TRUE)

m5 = matrix(rep(c(10,20,30)), 3, 3);

m6 = matrix(rep(c(10,20,30), each = 3), 3, 3)

m5[3,3]

m5[2,3]; m5[3,2]


a1 = array(1:12, dim = c(2,3,2))

a2 = array(seq(from=50, to=16, by = -2), dim= c(3,2,3))

a3 = array(rep(c(10,20,30)),dim = c(2,3,4))

a4 = array(rep(c(10,20,30), each=8),dim = c(2,3,4))

l1 = list(name="ABC", modules=4, marks = c(80,90,75, 88))
l1$name
l1[[3]][4]
l1$marks[4]

l2 = list(course="BABI", no.students = 10, marks=c(60,70,85,90,40,30,35,65,70,95))
l2$marks[6]
l2[[3]][6]

v1 = c(1,3,2,4,5,2,1,4,3);
f1 = factor(v1, levels=c(1,2,3,4,5), labels=c("Chennai","Mumbai","Pune","Bangalore","Gurgaon"))

v2 = c(1,2,2,3,3)
f2 = factor(v2, levels=c(1,2,3), labels=c("Male","Female","Trans-Gender"))


employee = c('ABC','DEF','GHI')
salary = c(21000,23400,26300)
startdate = as.Date(c("2010-01-10", "2008-09-22", "2007-03-07"))

employee.data = data.frame(employee, salary, startdate)


model = c("Mazda", "Chevrolette", "swift")
cylinder = c(6,8,5)
launchdate = as.Date(c("2010-10-25","2015-01-20","2001-05-25"))
price = c(570000,550000,650000)
vtype = c(3,1,2)
type = factor(vtype, levels=c(1,2,3), labels=c("Mini", "Micro", "Sedan"))

car.data = data.frame(model, cylinder, launchdate, price, type)
str(car.data)


attach(pressure)

plot(temperature, pressure)
