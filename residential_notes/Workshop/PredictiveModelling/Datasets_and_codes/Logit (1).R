


#Some libraries

library(car)
library(caret)
library(class)
library(devtools)
library(e1071)
library(ggplot2)
library(Hmisc)

library(MASS)
library(nnet)
library(plyr)
library(pROC)
library(psych)
library(scatterplot3d)
library(SDMTools)
library(dplyr)
library(ElemStatLearn)
library(rpart)
library(rpart.plot)
library(randomForest)
library(neuralnet)

setwd( "/Users/bappa/Desktop/Analytics.GLIM/PM.G.Feb.18")
Loan<-read.csv(file.choose(), header=T)
na.omit(Loan)

summary(Loan)
str(Loan)

levels(Loan$Purpose)
levels(Loan$Job)
#Define some dummies
Loan$Default<-ifelse(Loan$Status=="Default",1,0)
Loan$Female<-ifelse(Loan$Gender=="Female",1,0)
Loan$Management<-ifelse(Loan$Job=="Management",1,0)
Loan$Skilled<-ifelse(Loan$Job=="skilled",1,0)
Loan$Unskilled<-ifelse(Loan$Job=="unskilled",1,0)

levels(Loan$Credit.History)
Loan$CH.Poor<-ifelse(Loan$Credit.History=="Poor",1,0)
Loan$CH.critical<-ifelse(Loan$Credit.History=="critical",1,0)
Loan$CH.good<-ifelse(Loan$Credit.History=="good",1,0)
Loan$CH.verygood<-ifelse(Loan$Credit.History=="very good",1,0)
Loan$CH.verygood<-ifelse(Loan$Credit.History=="verygood",1,0)

Loan$Purpose.car<-ifelse(Loan$Purpose=="car",1,0)
Loan$Purpose.cd<-ifelse(Loan$Purpose=="consumer.durable",1,0)
Loan$Purpose.education<-ifelse(Loan$Purpose=="education",1,0)
Loan$Purpose.personal<-ifelse(Loan$Purpose=="personal",1,0)


#Partitioning Data Sets
#Partition train and val
#We will use this throughout so that samples are comparable
set.seed(1234)
pd<-sample(2,nrow(Loan),replace=TRUE, prob=c(0.7,0.3))

train<-Loan[pd==1,]
val<-Loan[pd==2,]


sum(Loan$Default)
sum(val$Default)
sum(train$Default)
######





#Logistic Regression

#install.packages(c("SDMTools","pROC", "Hmisc"))
library(SDMTools)
library(pROC)
library(Hmisc)

#data frame
train.logit<-train[,c(2,4:6,9,11:24)]
val.logit<-val[,c(2,4:6,9,11:24)]
####
#Normalize?

####




train.2fact<-train[,c(4,5,12)]

val.2fact<-val[,c(4,5,12)]

write.csv(train.2fact,"train.2fact.csv")

###LPM
lpm.2<-lm(Default~Credit.Score+Work.Exp,train.2fact)
summary(lpm.2)


###LOGIT
logit.2.var<-glm(Default~Credit.Score+Work.Exp,train.2fact,family=binomial)
summary(logit.2.var)

# Visualising the Training set results
library(ElemStatLearn)
set = train.2fact
X1 = seq(min(set[, 1])-0.05 , max(set[, 1])+0.1 , by = 0.05)
X2 = seq(min(set[, 2]) -0.1, max(set[, 2])+0.1 , by = 0.1)
grid_set = expand.grid(X1, X2)
colnames(grid_set) = c('Work.Exp', 'Credit.Score')
prob_set = predict(logit.2.var, type='response',newdata = grid_set)
y_grid=ifelse(prob_set>0.5,1,0)
plot(set[, -3],
     main = 'Logistic (Train set)',
     xlab = 'Work.Exp', ylab = 'Credit.Score',
     xlim = range(X1), ylim = range(X2))
contour(X1, X2, matrix(as.numeric(y_grid), length(X1), length(X2)), add = TRUE)
points(grid_set, pch = '.', col = ifelse(y_grid == "1", 'blue', 'red'))
points(set, pch = 21, bg = ifelse(set[, 3] == "1", 'green', 'black'))


#Now for test
# Visualising the Test set results
library(ElemStatLearn)
set = val.2fact
X1 = seq(min(set[, 1])-0.1 , max(set[, 1])+0.1 , by = 0.01)
X2 = seq(min(set[, 2]) -0.1, max(set[, 2])+0.1 , by = 0.1)
grid_set = expand.grid(X1, X2)
colnames(grid_set) = c('Work.Exp', 'Credit.Score')
prob_set = predict(logit.2.var, type='response',newdata = grid_set)
y_grid=ifelse(prob_set>0.5,1,0)
plot(set[, -3],
     main = 'Logistic (Test set)',
     xlab = 'Work.Exp', ylab = 'Credit.Score',
     xlim = range(X1), ylim = range(X2))
contour(X1, X2, matrix(as.numeric(y_grid), length(X1), length(X2)), add = TRUE)
points(grid_set, pch = '.', col = ifelse(y_grid == "1", 'blue', 'red'))
points(set, pch = 21, bg = ifelse(set[, 3] == "1", 'green', 'black'))

####Decision Boundary?

#-0.20151 *CS+ 0.23861 *WE+11.66264>=0
#CS<=57.87623+1.18411WE
#11.66264/0.20151
#0.23861/0.20151
####Logit on all variables

###Try1
Logit.eq.1<-Default  ~ Credit.Score+ Loan.Offered+Work.Exp+  Own.house+ Dependents+Female+Management+Skilled+Unskilled+CH.Poor+ CH.critical+CH.good+ CH.verygood  +Purpose.car+
  Purpose.cd+Purpose.education+Purpose.personal

Logit.1 <- glm(Logit.eq.1  , train.logit, family = binomial)
summary(Logit.1)

###Try2
Logit.eq.2<-Default  ~ Credit.Score+ Work.Exp+ Dependents+Dependents+Female+Skilled+CH.verygood
 

Logit.2 <- glm(Logit.eq.2  , train.logit, family = binomial)
summary(Logit.2)
vif(Logit.2)


#####Retain only significant ones



Logit.eq.final<-Default  ~ Credit.Score+  Dependents+Female+Skilled+CH.verygood

Logit.final <- glm(Logit.eq.final   , train.logit, family = binomial)
summary(Logit.final)
vif(Logit.final)

varImp(Logit.final)
pred.logit.final <- predict.glm(Logit.final, newdata=val.logit, type="response")
pred.logit.final


val.logit$pred<-predict.glm(Logit.final, newdata=val.logit, type="response")

#Classification
tab.logit<-table(val.logit$Default,pred.logit.final>0.5)
tab.logit
#Logit
accuracy.logit<-sum(diag(tab.logit))/sum(tab.logit)
accuracy.logit

library(ROCR)
head(pred.logit.final)
hist(pred.logit.final)
#Plot


qplot(Credit.Score,pred,colour=Default, data=val.logit)

##ROC.LOGIT
#tpr (sensitivity): Percentage of 1s correctly identified
#fpr: Percentage of 1s ignored


pred<-predict(Logit.final,val.logit, type='response')
pred<-prediction(pred, val.logit$Default)
roc<-performance(pred,"tpr", "fpr")
plot(roc)
auc<-performance(pred,"auc")
auc
auc<-unlist(slot(auc,"y.values"))
auc

###Decision Rule

###Algorithm
val.logit$Decision<-ifelse(val.logit$pred>0.5,"NO","YES")

#Scorecard?


#####Logit or LPM?

LPM1<-Default  ~ Credit.Score+ Loan.Offered+Work.Exp+  Own.house+ Dependents+Female+Management+Skilled+Unskilled+CH.Poor+ CH.critical+CH.good+ CH.verygood  +Purpose.car+
  Purpose.cd+Purpose.education+Purpose.personal

LPM.1 <- lm(LPM1  , train.logit)
summary(LPM.1)




LPM.eq.final<-Default  ~ Credit.Score+  Own.house+ Dependents+Female+Skilled+CH.Poor+ Purpose.car

LPM.final <-lm(LPM.eq.final  , train.logit)
summary(LPM.final)
vif(LPM.final)


pred.LPM.final <- predict.glm(LPM.final, newdata=val.logit, type="response")
pred.LPM.final


val.logit$pred.LPM<-predict.glm(LPM.final, newdata=val.logit, type="response")

#Classification
tab.LPM<-table(val.logit$Default,pred.LPM.final>0.5)
tab.LPM
#Logit
accuracy.LPM<-sum(diag(tab.LPM))/sum(tab.LPM)
accuracy.LPM

####ROC.LPM

##ROC


pred<-predict(LPM.final,val.logit, type='response')
pred<-prediction(pred, val.logit$Default)
roc<-performance(pred,"tpr", "fpr")
plot(roc)
auc<-performance(pred,"auc")
auc
auc<-unlist(slot(auc,"y.values"))
auc


#####LDA

lda.2<-lda(Default~Credit.Score+Work.Exp,train.2fact)
lda.2

#pedict
y_pred.LDA<-predict(lda.2,newdata=train.2fact[-3])
y_pred.LDA<-y_pred.LDA$class
#y_pred.DT<-ifelse(y_pred.DT>0.5,1,0)
#Confusion matrix
cm.train.LDA=table(train.2fact[,3],y_pred.LDA)
cm.train.LDA

####Val
y_pred.LDA<-predict(lda.2,newdata=val.2fact[-3])
y_pred.LDA<-y_pred.LDA$class
#y_pred.DT<-ifelse(y_pred.DT>0.5,1,0)
#Confusion matrix
cm.val.LDA=table(val.2fact[,3],y_pred.LDA)
cm.val.LDA


###FULL MODEL

LDA<-Default  ~ Credit.Score+ Loan.Offered+Work.Exp+  Own.house+ Dependents+Female+Management+Skilled+Unskilled+CH.Poor+ CH.critical+CH.good+ CH.verygood  +Purpose.car+
  Purpose.cd+Purpose.education+Purpose.personal

lda.final<-lda(LDA,data=train.logit)
lda.final


#pedict
y_pred.LDA<-predict(lda.final,newdata=val.logit)
y_pred.LDA<-y_pred.LDA$class

val.logit$ldapred<-y_pred.LDA

#Confusion matrix
cm.test.LDA=table(val.logit$Default,y_pred.LDA)
cm.test.LDA

