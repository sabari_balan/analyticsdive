# Install missing required packages.
packages <- c("dplyr")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))
}

#load all the required packages.
lapply(packages, require, character.only = T)

#set working directory to current directory where the script is.
setwd(dirname(rstudioapi::getSourceEditorContext()$path))




