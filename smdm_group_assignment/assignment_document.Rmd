---
title: "SMDM_Assignment_Group8"
author: "Sabari Balan R, Srinivetha, Geheni, Padmanabhan, Gowtham"
date: "19/07/2019"
output:
  word_document: default
  pdf_document: default
---
This is where you put explanation to install and load
```{r , ech}
# Install missing required packages.
packages <- c("xlsx","car","rstudioapi")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))
}
```
```{r}
#load all the required packages.
lapply(packages, require, character.only = T)
```
Explain how the data is fetched form Excel
```{r}
#Initializing file name to be read. (file is in the same directory as the script)
file_name = file.path("./assignment_data.xlsx")

#using xlsx instead of tabulizer package to read directly from pdf for simplicity
data = read.xlsx(file_name, sheetIndex = 1)

#attaching data to the environment to create independent variables.
attach(data)
```
Explain the boxplot
```{r boxplot, plot, fig.height=8, fig.width=10}
#boxplot to check the consistency of the data under consideration.
boxplot(Old.Scheme, New.Scheme, horizontal = T, 
        xlab = "Sales Output (in 1000s)", ylab = "Scheme",
        names = c("Old Scheme", "New Scheme"),
        col = c("cyan","blue"))

plot(Old.Scheme, New.Scheme)
abline(a=0, b=1)
```
Inference from the boxplot.

Intro to the histogram of Old Scheme
```{r, fig.width=10}
#histogram to visually check for the normality of the data under consideration
hist(Old.Scheme, xlab = "Sales Output of Old Scheme", col = "cyan", 
     main = "Histogram of Sales Output of Old Scheme")
```
Inference from Histogram of Old Scheme
Intro to Histrogram of new Scheme
```{r, fig.width=10}
hist(New.Scheme, xlab = "Sales Output of New Scheme", col = "blue",
     main = "Histogram of Sales Output of New Scheme")
```
Inference from Histogram of New Scheme
```{r}
oldMean = mean(Old.Scheme)
oldVar = var(Old.Scheme)
oldSD = sd(Old.Scheme)
```

```{r}
newMean = mean(New.Scheme)
newVar = var(New.Scheme)
newSD = sd(New.Scheme)
```
Why do you have to calculate difference of values ?
```{r}
###################################################################################
#getting the difference vector from between the new scheme and old scheme.
diff=c()

for(i in 1:30){
  diff = c(diff,New.Scheme[i] - Old.Scheme[i])
}
###################################################################################
```
Intro to histogram of difference in sales output.
```{r, fig.width=10}
# histogram to visually check the normality of the difference vector.
hist(diff, xlab = "Difference in Sales Output between New Scheme and Old Scheme", col = "Green",
     main="Histogram of Difference in Sales Output")
```
Inference from histogram of difference in sales output.
Intro to Shapiro's test.
```{r}
# Shapiro's test to confirm the normality of the difference vector with a 95% confidence level.
# H0: The difference is Normal
# H1: The difference is not normal
shapiro.test(diff)
```
Inference from shapiro's test.
Why group recors ? for leveneTest !

Why Levene's test ?
```{r}
# Performing Levene's Test to conform the equality of variances.
allRecords = c(New.Scheme, Old.Scheme);
groups <- as.factor(c(rep(1, length(New.Scheme)), rep(2, length(Old.Scheme))))

# Levene's Test
# H0: Variances of New.Scheme and Old.Scheme are equal
# H1: Variances of New.Scheme and Old.Scheme are not equal
leveneTest(allRecords, groups)
```
Infrerence from Levene's test.
Intro to 2 sample T test.
```{r}
#performing a 2 sample Student's T Test for significant raise in Outputs
# H0: mu <= 0
# H1: mu > 0
t.test(New.Scheme, Old.Scheme, alternative = "greater", mu = 0, paired = T, conf.level = 0.95)
```
Inference from Two sample T Test.
Changed mean to 5000 and performing  T test again.
```{r}
#performing a 2 sample T Test to check if Titan could break even
# H0: mu <= 5 # 5000 ignoring thousands
# H1: mu > 5
t.test(New.Scheme, Old.Scheme, alternative = "greater", mu = 5, paired = T, conf.level = 0.95)
```
Inference from  T test for mean = 5

Calculation and explanation for power of the test.
```{r}
######### Calculating Power of the Test for Errors ###########################################
delta = newMean - oldMean

# total number of sample records in observation.
n = 30 

#Calculate pooled standard deviation between old and new schemes (paired samples)
pooledSD = sqrt((n * (newVar + oldVar))/(n*2 - 2)) 

# Power of test to check if Titan could break even
power.t.test(n, delta, pooledSD, type = "paired", alternative = "one.sided")
```
Inference from the power of the test.

What do you need to increase the power of the test to 80%
```{r}
# Sample required to bring the power to at least 80%
power.t.test(power = 0.80, delta = delta, sd = pooledSD, type = "paired", alternative = "one.sided")
```
Inference from the inverse power calculation.
```{r}
detach(data)
```

Conclusions for the questions :

